<?php 
  drupal_add_js('misc/form.js');
  drupal_add_js('misc/collapse.js');
?>




<?php 
  $profile_uid = $elements['#account']->uid;
  $account = user_load($profile_uid); 
  $profile = profile2_load_by_user($account); 
?>
  

<fieldset class="collapsible collapsed">
<legend><span class="fieldset-legend">Profile Information</span></legend>
<div class="fieldset-wrapper">
<div class="fieldset-description clearfix">

<ul class="profile-column profile-personal">
<li><?php print drupal_render(field_view_field('profile2', $profile['profile'], 'field_user_image', array('label'=>'hidden','settings' => array('label'=>'hidden', 'image_style' => 'thumbnail')))); ?></li>
<li><?php print drupal_render(field_view_field('profile2', $profile['profile'], 'field_ss')); ?></li>
<li><?php print drupal_render(field_view_field('profile2', $profile['profile'], 'field_dob')); ?></li>
<li><?php print drupal_render(field_view_field('profile2', $profile['profile'], 'field_gender')); ?></li>
<li><?php print drupal_render(field_view_field('profile2', $profile['profile'], 'field_address')); ?></li>
<li><?php print drupal_render(field_view_field('profile2', $profile['profile'], 'field_city')); ?></li>
<li><?php print drupal_render(field_view_field('profile2', $profile['profile'], 'field_zip')); ?></li>
<li><?php print drupal_render(field_view_field('profile2', $profile['profile'], 'field_phone')); ?></li>
<li>
	<div class="field field-name-field-zip field-type-text field-label-above">
		<div class="field-label">E-Mail:</div><div class="field-items"><?php print $elements['#account']->mail; ?></div>
	</div>
</li>
</ul>

<ul class="profile-column profile-worksite">
<li><?php print drupal_render(field_view_field('profile2', $profile['profile'], 'field_district')); ?></li>
<li><?php print drupal_render(field_view_field('profile2', $profile['profile'], 'field_school')); ?></li>
<li><?php print drupal_render(field_view_field('profile2', $profile['profile'], 'field_school_address')); ?></li>
<li><?php print drupal_render(field_view_field('profile2', $profile['profile'], 'field_school_city')); ?></li>
<li><?php print drupal_render(field_view_field('profile2', $profile['profile'], 'field_school_zip')); ?></li>
<li><?php print drupal_render(field_view_field('profile2', $profile['profile'], 'field_school_phone')); ?></li>
<li><?php print drupal_render(field_view_field('profile2', $profile['profile'], 'field_site_administrator')); ?></li>
<li><?php print drupal_render(field_view_field('profile2', $profile['profile'], 'field_subject')); ?></li>
<li><?php print drupal_render(field_view_field('profile2', $profile['profile'], 'field_grade')); ?></li>
<li><?php print drupal_render(field_view_field('profile2', $profile['profile'], 'field_hire_date')); ?></li>
</ul>

<ul class="profile-column profile-professional">
<li><?php print drupal_render(field_view_field('profile2', $profile['profile'], 'field_induction_program')); ?></li>
<li><?php print drupal_render(field_view_field('profile2', $profile['profile'], 'field_date_noe_received')); ?></li>
<li><?php print drupal_render(field_view_field('profile2', $profile['profile'], 'field_participation_year')); ?></li>
<li><?php print drupal_render(field_view_field('profile2', $profile['profile'], 'field_program_status')); ?></li>
<li><?php print drupal_render(field_view_field('profile2', $profile['profile'], 'field_credential')); ?></li>
<li><?php print drupal_render(field_view_field('profile2', $profile['profile'], 'field_credential_term')); ?></li>
<li><?php print drupal_render(field_view_field('profile2', $profile['profile'], 'field_credential_type')); ?></li>
<li><?php print drupal_render(field_view_field('profile2', $profile['profile'], 'field_credential_expiration')); ?></li>
<li><?php print drupal_render(field_view_field('profile2', $profile['profile'], 'field_pt_support_provider')); ?></li>
<li><?php print drupal_render(field_view_field('profile2', $profile['profile'], 'field_on_leave', array('label'=>'hidden'))); ?></li>

<?php if (($profile['btsa_admin_info']) && user_access('view any btsa_admin_info profile')): ?>    
<li><?php print drupal_render(field_view_field('profile2', $profile['btsa_admin_info'], 'field_pt_reader')); ?></li>
<?php endif; ?>
</ul>
</div><!-- /.fieldset-description -->

<div class="fieldset-admin-notes">
<?php if (($profile['btsa_admin_info']) && user_access('view any btsa_admin_info profile')): ?>  
<div><?php print drupal_render(field_view_field('profile2', $profile['btsa_admin_info'], 'field_administration_notes')); ?></div>
<?php endif; ?>
</div>


</div><!-- /.fieldset-wrapper -->
</fieldset>
  

  
 
 
 
 
 
 
